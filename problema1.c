#include <stdio.h>

#define diff(a, b) a - b

int calc_tiredness(int x1, int x2) {
	int
		i = 0,
		increment,
		tiredness = 0,
		tiredness_rate = 1;

	// Assuming diff won't be 0 initially
	increment = (diff(x1, x2) < 0) ? 1 : -1;

	// `i` is used as a "alternate operator"
	while (diff(x1, x2) != 0) {
		tiredness += tiredness_rate;
		i ^= 1;

		if (i == 1) {
			x1 += increment;
		} else {
			x2 -= increment;
			tiredness_rate++;
		}
	}

	return tiredness;
}

int main(int argc, char *argv[]) {
	int x1, x2;

	scanf("%d", &x1);
	scanf("%d", &x2);

	printf("%d\n", calc_tiredness(x1, x2));

	return 0;
}