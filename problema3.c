#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_VAL 109
#define NUMB_OF_POSSIBILITIES 2*MAX_VAL + 1

int q_compare(const void *a, const void *b) {
	return *(int *)a - *(int *)b;
}

/* 
	Strategy:
	- Sort the sequence, so it is possible to do binary searches
	- Try various combinations (order is relevant) with some optmizations (avoid repeated combinations)
 */

// `freq_id` helps to find unique terms. useful when there's more than one term with the same value
int search_fib_sequence_length(int *seq, int size, int i, int j) {
	int
		longest_prefix = 0,
		current_length = 2,
		next = seq[i] + seq[j],
		prev = seq[j],
		freq_id[size],
		*curr; // current

		memset(freq_id, 0, sizeof(freq_id));
		freq_id[i]++;
		freq_id[j]++;

	do {
		curr = bsearch(&next, seq, size, sizeof(int), q_compare);

		// if found next term
		if (curr != NULL) {
			// `curr - seq` gives the index of `curr`

			// try repositioning to first term with equal value since bsearch does
			// not specify the element for multiple elements that match the key
			while (curr - seq > 0 && curr[-1] == *curr) curr--;

			// then reposition to the next term which hasn't been used yet
			while (curr - seq < size-1 && curr[1] == *curr && freq_id[curr - seq] > 0) curr++;

			// try to find more terms if possible
			if (freq_id[curr - seq]++ == 0) {
				next = prev + *curr;
				prev = *curr;

				if (longest_prefix < ++current_length) {
					longest_prefix = current_length;
				}
			} else {
				curr = NULL;
			}
		}
	} while (curr != NULL);

	return longest_prefix;
}

// `freq_val1` and `freq_val2` helps to avoid repetitive processes
int longest_fib_prefix(int *seq, int size) {
	int i, j;
	int longest_prefix = 0, current_length;
	int freq_val1[NUMB_OF_POSSIBILITIES] = {0};
	int freq_val2[NUMB_OF_POSSIBILITIES];

	if (size <= 2) return size;

	qsort(seq, size, sizeof(int), q_compare);

	for (i = 0; i < size; i++) {
		if (freq_val1[seq[i]+MAX_VAL]++ == 0) {
			memset(freq_val2, 0, sizeof(freq_val2));

			for (j = 0; j < size; j++) {
				if (i != j && freq_val2[seq[j]+MAX_VAL]++ == 0) {

					current_length = search_fib_sequence_length(seq, size, i, j);

					if (longest_prefix < current_length) {
						longest_prefix = current_length;
					}
				}
			}
		}
	}

	return longest_prefix;
}

int main(int argc, char *argv[]) {
	int i;
	int n, *seq;

	scanf("%d", &n);

	seq = malloc(sizeof(int) * n);

	for (i = 0; i < n; i++) {
		scanf("%d", &seq[i]);
	}

	printf("%d\n", longest_fib_prefix(seq, n));

	free(seq);
	return 0;
}