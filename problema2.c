#include <stdio.h>
#include <stdlib.h>

#define BOARD_SIZE 3

// Boolean
enum {
	FALSE,
	TRUE
};

// ST: "firST player"
// ND: "secoND player"
enum {
	UNSET = -1,
	ST,
	ND,
	INVALID,
	ST_WINS,
	ND_WINS,
	DRAW
};

int *read_board() {
	int i;
	char c;
	int *board = malloc(sizeof(int) * BOARD_SIZE*BOARD_SIZE);

	for (i = 0; i < BOARD_SIZE*BOARD_SIZE; i++) {
		do {
			c = getchar();
		} while (c == '\n');

		// maps the board as follow:
		// 'X':  1
		// '0': -1
		// '.':  0
		switch(c) {
			case 'X':
				board[i] = 1;
				break;
			case '0':
				board[i] = -1;
				break;
			case '.':
				board[i] = 0;
				break;
		}
	}

	return board;
}

void print_board(int *board) {
	int i;

	for (i = 0; i < BOARD_SIZE*BOARD_SIZE; i++) {
		switch(board[i]) {
			case 1:
				printf("X");
				break;
			case -1:
				printf("0");
				break;
			case 0:
				printf(".");
				break;
		}

		if (i % BOARD_SIZE == BOARD_SIZE-1) {
			printf("\n");
		} else {
			printf(" ");
		}
	}
}

int array_sum(int *array, int size) {
	int i, sum = 0;

	for (i = 0; i < size; i++) sum += array[i];

	return sum;
}

int is_game_over(int *board) {
	int i;

	// If there is at least one '.', then the game is still not over
	for (i = 0; i < BOARD_SIZE*BOARD_SIZE; i++) {
		if (board[i] == 0) return FALSE;
	}

	return TRUE;
}

// -1 (UNSET) return means there is no winner yet nor the game is over
// this function also check if the board is invalid.
int get_winner(int *board) {
	int i, sum, winner = UNSET;

	// check if there is no play out of turn
	sum = array_sum(board, BOARD_SIZE*BOARD_SIZE);
	if (sum != 0 && sum != 1) return INVALID;

	// if the sum's abs-value equals to `BOARD_SIZE`, then there's a winner
	for (i = 0; i < BOARD_SIZE; i++) {

		// row sum
		if (abs(array_sum(board + (i*BOARD_SIZE), BOARD_SIZE)) == BOARD_SIZE) {
			if (winner == UNSET) {
				// the first value indicates who is the winner
				winner = (board[i*BOARD_SIZE] == 1) ? ST_WINS : ND_WINS;
			} else {
				// in case there's 2 different winners...
				int second_winner = (board[i*BOARD_SIZE] == 1) ? ST_WINS : ND_WINS;
				if (winner != second_winner) return INVALID;
			}
		}

		// column sum
		if (abs(board[i] + board[i + BOARD_SIZE] + board[i + 2*BOARD_SIZE]) == BOARD_SIZE) {
			if (winner == UNSET) {
				winner = (board[i] == 1) ? ST_WINS : ND_WINS;
			} else {
				int second_winner = (board[i] == 1) ? ST_WINS : ND_WINS;
				if (winner != second_winner) return INVALID;
			}
		}
	}

	// diagonals
	// (general rule)
	// board[i + i*BOARD_SIZE]
	// board[(i+1) * BOARD_SIZE - (i+1)]
	if (abs(board[0] + board[4] + board[8]) == BOARD_SIZE
		|| abs(board[2] + board[4] + board[6]) == BOARD_SIZE) {
		
		if (winner == UNSET) {
			// using `board[4]` as reference since it appears in both diagonals
			winner = (board[4] == 1) ? ST_WINS : ND_WINS;
		} else {
			int second_winner = (board[4] == 1) ? ST_WINS : ND_WINS;
			if (winner != second_winner) return INVALID;
		}
	}

	if (is_game_over(board) && winner == UNSET) return DRAW;
	
	return winner;
}

int get_next_player(int *board) {
	int winner;
	
	if ((winner = get_winner(board)) == UNSET) {
		if (array_sum(board, BOARD_SIZE*BOARD_SIZE) == 0) {
			// X turn
			return ST;
		} else {
			// 0 turn
			return ND;
		}
	} else {
		return winner;
	}
}

int main(int argc, char *argv[]) {
	int *board;

	board = read_board();

	switch(get_next_player(board)) {
		case ST:
			printf("primeiro\n");
			break;
		case ND:
			printf("segundo\n");
			break;
		case INVALID:
			printf("Inválido\n");
			break;
		case ST_WINS:
			printf("O primeiro jogador venceu\n");
			break;
		case ND_WINS:
			printf("O segundo jogador venceu\n");
			break;
		case DRAW:
			printf("Empate\n");
			break;
	}

	free(board);
	return 0;
}